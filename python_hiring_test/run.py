"""Main script for generating output.csv."""


def main():
    import pandas as pd
    data = pd.read_csv('C:\\Users\\sadhana reddy\\Desktop\\python_hiring_test\\python_hiring_test\\data\\raw\\pitchdata.csv')
    print(data.shape)
    print(data.head())

    
    print(data.apply(lambda x: len(x.unique())))
    print(data.columns)
    
    def group_filterP(i,s):
        grouped = data[(data['HitterSide'] == s)].groupby(i).sum()
        filtered = grouped[grouped['PA'] >= 25]
        return(stat(filtered))
    def group_filterH(i,s):
        grouped = data[(data['PitcherSide'] == s)].groupby(i).sum()
        filtered = grouped[grouped['PA'] >= 25]
        return(stat(filtered))

    def stat(H_sum):
        Average_RHP = round(H_sum['H']/H_sum['AB'],3)
        OBP = (H_sum['H']+H_sum['BB']+H_sum['HBP'])/(H_sum['AB']+H_sum['BB']+H_sum['HBP']+H_sum['SF'])
        SLG = H_sum['TB']/H_sum['AB']
        OBP_RHP = round(OBP,3)
        SLG_RHP = round(SLG,3)
        OPS_RHP = round(OBP + SLG,3)
        stats = pd.concat([Average_RHP,OBP_RHP,SLG_RHP,OPS_RHP], keys = (['AVG','OBP','SLG','OPS'])).reset_index()
        stats.shape
        return stats

    def Split(grp,s):
        if list(grp)[0] == 'H':
            a = 'P'
        elif list(grp)[0] == 'P':
            a = 'H'
        split = "vs " + s +'H'+ a
        return(split)

    def restructure(df,Subject,Split):
        RHP_df = pd.concat([pd.DataFrame( columns = ['Split','Subject']),df]) 
        RHP_df[['Split','Subject']] = [Split,Subject]
        RHP_df.columns = ['SubjectId','Split','Subject','Stat','Value']
        RHP_df = RHP_df[['SubjectId','Stat','Split','Subject','Value']]
        RHP_df.SubjectId = RHP_df.SubjectId.astype('int64')
        return(RHP_df)
    
    def concat(df,df1):
        final = pd.concat([df1,df], axis = 0)
        final = final.sort_values(['SubjectId', 'Stat', 'Split','Subject'], ascending= [True,True,True,True])
        return(final) 


    final = pd.DataFrame()
    Hitter_calc = ['HitterId','HitterTeamId']
    Side = ['R','L']
    for s in Side:
        for i in Hitter_calc:
            output = group_filterH(i,s)
            split = Split(i,s)
            output_df = restructure(output,i,split)
            final = concat(output_df,final)
    
    Pitcher_calc = ['PitcherId','PitcherTeamId']
    for s in Side:
        for i in Pitcher_calc:
            output = group_filterP(i,s)
            split = Split(i,s)
            output_df = restructure(output,i,split)
            final = concat(output_df,final)
    final.to_csv('C:\\Users\\sadhana reddy\\Desktop\\python_hiring_test\\python_hiring_test\\data\\processed\\output.csv',header = True,index = False)
    print(final.shape)
    
pass


if __name__ == '__main__':
    main()
